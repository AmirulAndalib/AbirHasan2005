<h1 align="left"><img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25px"> Hi, I'm Abir Hasan</h1>

## About Meh:
- Currently learning Python, JavaScript & Bash.
- A Muslim Boi 😇
- Sleeping iz Mai Lub 😪
    - But Bery Bezi 🤐
- Making some Telegram Bots & other iStupid Tools.
    - Dis makes me Happy 🤗
- Busy Student 😆
- Speaking Languages:
    - Bangla
    - English
    - Hindi
    - Banglish
    - Hinglish
    - Emglish
- [More on Telegram](https://t.me/AbirHasan2005/3)

<p align="left"> <a href="https://github.com/AbirHasan2005"><img src="https://komarev.com/ghpvc/?username=AbirHasan2005&label=Profile%20views&color=0e75b6&style=flat" alt="AbirHasan2005" /></a> </p>

<p align="left"> <a href="https://github.com/AbirHasan2005"><img src="https://github-profile-trophy.vercel.app/?username=AbirHasan2005" alt="AbirHasan2005" /></a> </p>


<h3 align="left">Languages:</h3>
<p align="left"> <a href="https://www.gnu.org/software/bash/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/gnu_bash/gnu_bash-icon.svg" alt="bash" width="40" height="40"/> </a> <a href="https://git-scm.com/" target="_blank"> <img src="https://github.com/Thomas-George-T/Thomas-George-T/raw/master/assets/git.svg" alt="git" width="40" height="40"/> </a> <a href="https://www.w3.org/html/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> <a href="https://www.python.org" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a> </p>

<h3 align="left">Libraries:</h3>
<p align="left"> <a href="https://www.selenium.dev" target="_blank"> <img src="https://raw.githubusercontent.com/detain/svg-logos/780f25886640cef088af994181646db2f6b1a3f8/svg/selenium-logo.svg" alt="selenium" width="40" height="40"/> </a> <a href="https://github.com/pyrogram/pyrogram" target="_blank"> <img src="https://raw.githubusercontent.com/pyrogram/logos/fe16a72cae833fcabf1f79ca0b33cee6af2f3bc3/logos/pyrogram.svg" alt="pyrogram" width="50" height="50"/> </a> </p>

<h3 align="left">Fav Databases:</h3>
<p align="left"> <a href="https://www.mongodb.com/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mongodb/mongodb-original-wordmark.svg" alt="mongodb" width="40" height="40"/> </a> <a href="https://www.mysql.com/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="40" height="40"/> </a> <a href="https://www.postgresql.org" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/postgresql/postgresql-original-wordmark.svg" alt="postgresql" width="40" height="40"/> </a> </p>

<h3 align="left">Tools:</h3>
<p align="left"> <a href="https://www.docker.com/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-original-wordmark.svg" alt="docker" width="40" height="40"/> </a> <a href="https://www.jetbrains.com/pycharm/" target="_blank"> <img src="https://github.com/devicons/devicon/raw/master/icons/pycharm/pycharm-original-wordmark.svg" alt="pycharm" width="40" height="40"/> </a> <a href="https://www.nginx.com" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/nginx/nginx-original.svg" alt="nginx" width="40" height="40"/> </a> </p>

<h3 align="left">Servers & Hosts:</h3>
<p align="left"> <a href="https://github.com/" target="_blank"> <img src="https://github.com/devicons/devicon/raw/master/icons/github/github-original-wordmark.svg" alt="github" width="40" height="40"/> </a> <a href="https://aws.amazon.com" target="_blank"> <img src="https://github.com/Thomas-George-T/Thomas-George-T/raw/master/assets/aws.svg" alt="aws" width="40" height="40"/> </a> <a href="https://azure.microsoft.com/en-in/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/microsoft_azure/microsoft_azure-icon.svg" alt="azure" width="40" height="40"/> </a> <a href="https://cloud.google.com" target="_blank"> <img src="https://www.vectorlogo.zone/logos/google_cloud/google_cloud-icon.svg" alt="gcp" width="40" height="40"/> </a> <a href="https://heroku.com" target="_blank"> <img src="https://github.com/Thomas-George-T/Thomas-George-T/raw/master/assets/heroku.svg" alt="heroku" width="40" height="40"/> </a> <a href="https://www.linux.org/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width="40" height="40"/> </a> </p>


[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=AbirHasan2005)](https://github.com/AbirHasan2005)

<p>&nbsp;<img align="center" src="https://github-readme-stats.vercel.app/api?username=AbirHasan2005&show_icons=true&locale=en" alt="AbirHasan2005" /></p>

<p><img align="center" src="https://github-readme-streak-stats.herokuapp.com/?user=AbirHasan2005&" alt="AbirHasan2005" /></p>


### :zap: Recent Activities:

<!--START_SECTION:activity-->
1. ❗️ Closed issue [#13](https://github.com/AbirHasan2005/ShellPhish/issues/13) in [AbirHasan2005/ShellPhish](https://github.com/AbirHasan2005/ShellPhish)
2. 🗣 Commented on [#13](https://github.com/AbirHasan2005/ShellPhish/issues/13) in [AbirHasan2005/ShellPhish](https://github.com/AbirHasan2005/ShellPhish)
3. 🎉 Merged PR [#3](https://github.com/Discovery-Projects/DMCA-Delete-Bot/pull/3) in [Discovery-Projects/DMCA-Delete-Bot](https://github.com/Discovery-Projects/DMCA-Delete-Bot)
4. 🗣 Commented on [#3](https://github.com/Discovery-Projects/DMCA-Delete-Bot/issues/3) in [Discovery-Projects/DMCA-Delete-Bot](https://github.com/Discovery-Projects/DMCA-Delete-Bot)
5. 🎉 Merged PR [#1](https://github.com/Discovery-Projects/DMCA-Delete-Bot/pull/1) in [Discovery-Projects/DMCA-Delete-Bot](https://github.com/Discovery-Projects/DMCA-Delete-Bot)
<!--END_SECTION:activity-->

## Follow on:
[![Abir Hasan](https://img.icons8.com/fluent/48/000000/twitter.png)][twitter]
[![Abir Hasan](https://img.icons8.com/fluent/48/000000/instagram-new.png)][instagram]
[![Abir Hasan](https://img.icons8.com/fluent/48/000000/telegram-app.png)][telegram]
[![Nahid Hasan Abir](https://img.icons8.com/fluent/48/000000/facebook-new.png)][facebook]

## Support Group:
<a href="https://t.me/linux_repo"><img src="https://img.shields.io/badge/Linux%20Repositories-Join%20Telegram%20Group-blue.svg?logo=telegram"></a>

## Telegram Bots Channel:
<a href="https://t.me/linux_repo"><img src="https://img.shields.io/badge/Discovery%20Updates-Join%20Telegram%20Channel-blue.svg?logo=telegram"></a>

[twitter]: https://twitter.com/AbirHasan2005
[instagram]: https://instagram.com/AbirHasan2005
[telegram]: https://t.me/AbirHasan2005
[facebook]: https://facebook.com/AbirHasan2005
